import flask

from .config import DefaultConfig


def create_app():
    app = flask.Flask(__name__)
    app.config.from_object(DefaultConfig())

    from .database import db
    db.init_app(app)
    from .views import pokemon_bp
    app.register_blueprint(pokemon_bp)

    return app
