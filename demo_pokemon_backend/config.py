class DefaultConfig:
    SQLALCHEMY_DATABASE_URI = f"sqlite:///database.sqlite"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
