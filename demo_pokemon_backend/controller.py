import csv
import importlib.resources

from . import resources
from .database import db
from .enums import PokemonType
from .models import Pokemon


def reset_db():
    db.drop_all()
    db.create_all()
    with importlib.resources.open_text(resources, "Pokemon.csv") as f:
        reader = csv.DictReader(f)
        for row in reader:
            new_pokemon = Pokemon(
                id=int(row["#"]),
                name=row["Name"],
                type_one=PokemonType(row["Type 1"]),
                type_two=PokemonType(row["Type 2"] or None),
                total=int(row["Total"]),
                hp=int(row["HP"]),
                attack=int(row["Attack"]),
                defense=int(row["Defense"]),
                special_attack=int(row["Sp. Atk"]),
                special_defense=int(row["Sp. Def"]),
                speed=int(row["Speed"]),
                generation=int(row["Generation"]),
                is_legendary=(row["Legendary"] == "True")
            )
            db.session.add(new_pokemon)
    db.session.commit()
