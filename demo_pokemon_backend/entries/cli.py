import click

from demo_pokemon_backend import create_app, controller


@click.group(name="demo-pokemon-backend")
def run():
    pass


@run.command()
def serve():
    app = create_app()
    app.run()


@run.command()
def reset():
    app = create_app()
    with app.app_context():
        controller.reset_db()
    click.echo("Successfully reset the database!")


if __name__ == "__main__":
    run()
