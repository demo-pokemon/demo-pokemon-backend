from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method

from .database import db
from .enums import PokemonType


class Pokemon(db.Model):
    name = db.Column(db.String, primary_key=True)
    id = db.Column(db.Integer, nullable=False)
    type_one = db.Column(db.Enum(PokemonType), nullable=False)
    type_two = db.Column(db.Enum(PokemonType))
    total = db.Column(db.Integer, nullable=False)
    hp = db.Column(db.Integer, nullable=False)
    attack = db.Column(db.Integer, nullable=False)
    defense = db.Column(db.Integer, nullable=False)
    special_attack = db.Column(db.Integer, nullable=False)
    special_defense = db.Column(db.Integer, nullable=False)
    speed = db.Column(db.Integer, nullable=False)
    generation = db.Column(db.Integer, nullable=False)
    is_legendary = db.Column(db.Boolean, nullable=False)

    @hybrid_property
    def type(self):
        if self.type_two is None:
            return self.type_one
        else:
            return f"{self.type_one},f{self.type_two}"

    @hybrid_method
    def is_type(self, value):
        if isinstance(value, PokemonType):
            return self.type_one == value or self.type_two == value
        else:
            return self.type_one.value == value or self.type_two.value == value

    def __repr__(self):
        return f"<Pokemon {self.name}>"
