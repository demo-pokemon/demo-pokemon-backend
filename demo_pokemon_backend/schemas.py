from marshmallow import Schema, fields

from .utils import camelcase


class BaseSchema(Schema):
    def on_bind_field(self, field_name, field_obj):
        field_obj.data_key = camelcase(field_obj.data_key or field_name)


class PokemonSchema(BaseSchema):
    name = fields.Str(required=True)
    id = fields.Int(required=True)
    type = fields.Str(required=True)
    total = fields.Int(required=True)
    hp = fields.Int(required=True)
    attack = fields.Int(required=True)
    defense = fields.Int(required=True)
    special_attack = fields.Int(required=True)
    special_defense = fields.Int(required=True)
    speed = fields.Int(required=True)
    generation = fields.Int(required=True)
    is_legendary = fields.Bool(required=True)


pokemon_schema = PokemonSchema()
pokemons_schema = PokemonSchema(many=True)
