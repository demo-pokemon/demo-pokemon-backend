import flask

from .models import Pokemon
from .schemas import pokemon_schema, pokemons_schema

pokemon_bp = flask.Blueprint('pokemon', __name__)


@pokemon_bp.route("/pokemons", methods=["GET"])
def read_pokemons():
    pokemons = Pokemon.query.all()
    return flask.jsonify(pokemons_schema.dump(pokemons))


@pokemon_bp.route("/pokemons/count", methods=["GET"])
def count_pokemons():
    pokemons_count = Pokemon.query.count()
    return flask.jsonify(pokemons_count)


@pokemon_bp.route("/pokemons/<name>", methods=["GET"])
def read_pokemon(name):
    pokemon = Pokemon.query.get_or_404(name)
    return pokemon_schema.dump(pokemon)
