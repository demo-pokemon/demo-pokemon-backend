from setuptools import setup, find_packages

setup(
    name='demo-pokemon-backend',
    version='0.1.0',
    packages=find_packages(),
    url='https://gitlab.com/demo-pokemon/demo-pokemon-backend',
    license='MIT',
    author='kevin',
    author_email='kevinju0827@gmail.com',
    description='',
    install_requires=[
        'click',
        'Flask',
        'Flask-SQLAlchemy',
        'marshmallow',
        'marshmallow-enum'
    ],
    package_data={
        'demo_pokemon_backend': ["resources/*"]
    },
    entry_points={
        'console_scripts': [
            "demo-pokemon-backend = demo_pokemon_backend.entries.cli:run"
        ]
    }
)
